<?php

/**
 * Created by PhpStorm.
 * User: gladdos
 * Date: 26.04.2018
 * Time: 17:50
 */
include_once 'config.php';

class mysql
{
    function mysql()
    {
        $con = mysqli_connect(config::$host, config::$user, config::$pass, config::$db);
        mysqli_set_charset($con, config::$encoding);
        return $con;
    }
}
