<?php
/**
 * Created by PhpStorm.
 * User: gladdos
 * Date: 26.04.2018
 * Time: 18:09
 */

class config
{
    public static $host = 'localhost';
    public static $user = 'root';
    public static $pass = '';
    public static $db = 'hotlinemiami';
    public static $encoding = 'utf8mb4_general_ci';
}